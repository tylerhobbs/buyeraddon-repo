/*======================= Create AddOn Table ==========================*/
create table profound.AddOn(
ItmNum DECIMAL(5, 0) NOT NULL DEFAULT 0 ,
ChkDig DECIMAL(1,0) NOT NULL DEFAULT '',
PkSize char(10) NOT NULL DEFAULT '',
BoH char(1) NOT NULL DEFAULT '',
Descrip char(28) NOT NULL DEFAULT '',
PRIMARY KEY( ItmNum, ChkDig ) )
RCDFMT storesr;

LABEL ON TABLE profound.AddOn
	IS 'Buyer Add-Ons' ;

LABEL ON COLUMN profound.AddOn(
    ItmNum TEXT IS 'Item Code' ,
    ChkDig TEXT IS 'Check Digit',
    PkSize TEXT IS 'Pack Size',
	BoH TEXT IS 'Balance on Hand' ,
	Descrip TEXT IS 'Description'
    ) ;
/* ==============================================================*/    