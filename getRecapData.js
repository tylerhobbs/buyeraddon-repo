async function getRecapData() {
  console.log("getRecapData.js is running");

  let compareArray = [];
  let exportsArray = [];

  //get Token one time for the day for use in the storeClaimHistoryAPI
  let getToken = await pjs.sendRequest({
    method: "GET",
    uri: "http://localhost:8091/createToken/Tyler Hobbs",
    headers: {
      Accept: "application/json"
    },
    json: true,
    timeout: 8000,
    time: true
  });

  const token = getToken.token;

  //Get all markout claims that we are looking for from the clmhst table
  const clmhstData = pjs.query("select hclmst, hcldoc from pwafil.clmhst where hclmty = 7 and hclusr = 'JUNE2023' limit 50");

  clmhstData.forEach(clmhstRow => {
    let storeNumber = clmhstRow.hclmst;
    let documentNumber = clmhstRow.hcldoc;
    compareArray.push({ storeNumber, documentNumber });
  });

//   console.log(compareArray);

//for each record found, call the storeClaimHistoryAPI to get the recap data based on the store number and document number
  compareArray.forEach(compareRow => {

    let itemAPIResult = pjs.sendRequest({
        method: "POST",
        uri: "http://localhost:8091/storesClaimHistoryAPI", //PROD
        headers: { token: token },
        body: {
          StoreNumber: compareRow.storeNumber,
          ItemNumber: 0,
          InvoiceNumber: 0,
          ClaimNumber: 0,
          DocumentNumber: compareRow.documentNumber,
          ClaimDate: 0,
          OrderDate: 0
        },
        json: true,
        timeout: 50000,
        time: true
      });

    // console.log(itemAPIResult.array);

    // push each StoreNumber, ItemNumber, InvoiceNumber, ClaimNumber, DocumentNumber, ClaimDate, RecapNumber and RecapDate into the exportsArray
    exportsArray.push({
      StoreNumber: itemAPIResult.array[0].StoreNumber,
      ItemNumber: itemAPIResult.array[0].ItemNumber,
      InvoiceNumber: itemAPIResult.array[0].InvoiceNumber,
      ClaimNumber: itemAPIResult.array[0].ClaimNumber,
      ClaimDate: itemAPIResult.array[0].ClaimDate,
      DocumentNumber: itemAPIResult.array[0].DocumentNumber,
      RecapNumber: itemAPIResult.array[0].RecapNumber,
      RecapDate: itemAPIResult.array[0].RecapDate
    });

  });

    console.log(exportsArray);

}
exports.run = getRecapData;
