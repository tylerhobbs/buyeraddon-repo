var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
function importxlsx() {
    pjs.defineDisplay('display', 'XLSXtoPlanetPressFront.json');
  
    pjs.define('UploadInfo', {
      type: 'data structure',
      qualified: true,
      elements: {
        num_files: { type: 'zoned', length: 3, decimals: 0 },
        directory: { type: 'char', length: 256 },
        file: { type: 'char', length: 256 },
      },
    });
  
    pjs.define('siPrice', {
        type: 'packed decimal',
        length: 4,
        decimals: 2,
    });
    pjs.define('adEndDate', {
        type: 'string'
    });
    //----------------------------------------------------------------------------------------------------------------------------------------------
    if (typeof require !== 'undefined') XLSX = require('xlsx');
    var XLSX = require('xlsx');
  
    while (!Exit) {
      display.ExcelUpload.execute();
      var XLSX = require('xlsx');
  
      //get file name from upload info to read.
      let file = UploadInfo.file;
      file = file.trim();
      let numFiles = UploadInfo.num_files;
      let directory = UploadInfo.directory;
      fileName = directory.trim() + '/' + file;
      fileName = fileName.trim();
      //----------------------------------------------------------------------------------------------------------------------------------------------
      if (file.length > 0 && Enter) {
        console.log('Read pressed for file: ' + fileName);
  
        wb = XLSX.readFile(fileName, { cellDates: true });
        var ws = wb.Sheets['SIGN DATA'];
        var data = XLSX.utils.sheet_to_json(ws);
        //console.log(wb);
        console.log(data);
  
        var store = 000;
        var adNumber = 0;
        var itemCode = 0;
        var size = "";
        var description = "";
        var upc = 0000000000;
        var ytdMovement = 0;
        var siMulti = 0;
        siPrice = 0000;
        siPercent = "";
        var multi = 00;
        var price = 00;
        adEndDate = "dd/mm/yyyy";
        //var d = new Date();
        //var todaysDay = d.getDate();
        //var todaysMonth = d.getMonth() + 1;
        //var todaysYear = d.getFullYear();
        //var formatDate = dateToNumber(d);
  
        for (i = 0; i < data.length; i++) {
          if (data[i]['Store']) store = data[i]['Store'];
  
          if (data[i]['Ad Number']) adNumber = data[i]['Ad Number'];
  
          if (data[i]['Item Code']) itemCode = data[i]['Item Code'];
  
          if (data[i]['Size']) size = data[i]['Size'];
  
          if (data[i]['Description']) description = data[i]['Description'];
  
          if (data[i]['UPC']) upc = data[i]['UPC'];

          if (data[i]['YTD Mvt']) ytdMovement = data[i]['YTD Mvt'];

          if (data[i]['SI Multi']) siMulti = data[i]['SI Multi'];

          if (data[i]['SI Price']) siPrice = data[i]['SI Price'];

          if (data[i]['SI %']) siPercent = data[i]['SI %'];

          if (data[i]['Multi']) multi = data[i]['Multi'];

          if (data[i]['Price']) price = data[i]['Price'];

          if (data[i]['Ad End Date']) adEndDate = data[i]['Ad End Date'];

          //console.log("store: " + store, 
          //"adNumber: " + adNumber, 
          //"itemCode: " + itemCode, 
          //"size: " + size, 
          //"description: " + description, 
          //"upc: " + upc, 
          //"ytdMovement: " + ytdMovement, 
          //"siMulti: " + siMulti, 
          //"siPrice: " + siPrice, 
          //"siPercent: " + siPercent, 
          //"multi: " + multi, 
          //"price: " + price, 
          //"adEndDate: " + adEndDate
          //);
        }
        console.log(data);

        let request = new XMLHttpRequest();
        request.open("POST", "http://10.1.0.41:8080/APItest/");
        request.send();

        console.log(request);
        
        //var send;
        //send = pjs.sendRequest({
            //method: "POST",
            //uri: "http://10.1.0.41:8080/APItest/",
            //body: {data},
            //headers: {token: '2022020413170048d55ad0b495d3f5e2be10fb433b6340acc3ef9e67b7fb92e3b1343a81fa3f27'},
            //json: true,
            //timeout: 5000,
            //time: true,
      }
  
      if (showGrid) {
        doGrid();
      }
  
      function doGrid() {
        Exit = false;
        Exit2 = false;
  
        while (!Exit && !Exit2) {
          console.log('Show Grid clicked.');
          display.APIMPRTGrid.execute();
  
          // console.log(vendorNumber,
          //   invoiceNumber,
          //   typeCode,
          //   dueMonth,
          //   dueDay,
          //   dueYear,
          //   invoiceAmount,
          //   discountAmount,
          //   netAmount,
          //   todaysMonth,
          //   todaysDay,
          //   todaysYear,
          //   accountNumber,
          //   formatDate,
          //   moveFlag,);
  
          if (Exit2) {
            return;
          }
  
          if (clear) {
            pjs.query('delete from TYLER.APIMPRT');
            console.log('table cleared');
          }
        }
      }
    }
  }
  exports.run = importxlsx;
  
  function dateToNumber(theDate) {
    var year = theDate.getFullYear() % 2000;
    var month = theDate.getMonth() + 1;
    var day = theDate.getDate();
    var number = month * 10000 + day * 100 + year;
    return number;
  }
  