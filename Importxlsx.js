function importxlsx() {
  pjs.defineDisplay('display', 'UploadExcelFront.json');

  pjs.define('UploadInfo', {
    type: 'data structure',
    qualified: true,
    elements: {
      num_files: { type: 'zoned', length: 3, decimals: 0 },
      directory: { type: 'char', length: 256 },
      file: { type: 'char', length: 256 },
    },
  });

  pjs.define('invoiceAmount', {
    type: 'packed decimal',
    length: 6,
    decimals: 0,
  });
  pjs.define('accountNumber', {
    type: 'packed decimal',
    length: 4,
    decimals: 0,
  });
  //----------------------------------------------------------------------------------------------------------------------------------------------
  if (typeof require !== 'undefined') XLSX = require('xlsx');
  var XLSX = require('xlsx');

  while (!Exit) {
    display.ExcelUpload.execute();
    var XLSX = require('xlsx');

    //get file name from upload info to read.
    let file = UploadInfo.file;
    file = file.trim();
    let numFiles = UploadInfo.num_files;
    let directory = UploadInfo.directory;
    fileName = directory.trim() + '/' + file;
    fileName = fileName.trim();
    //----------------------------------------------------------------------------------------------------------------------------------------------
    if (file.length > 0 && Enter) {
      console.log('Read pressed for file: ' + fileName);

      wb = XLSX.readFile(fileName, { cellDates: true });
      var ws = wb.Sheets['Sheet1'];
      var data = XLSX.utils.sheet_to_json(ws);
      // console.log(wb);
      // console.log(data);

      var vendorNumber = 'VN';
      var invoiceNumber = 'IN';
      var typeCode = 0;
      var dueMonth = 00;
      var dueDay = 00;
      var dueYear = 0000;
      invoiceAmount = 000000000;
      var discountAmount = 000000;
      var netAmount = 000000000;
      // var invoiceMonth = '88';
      // var invoiceDay = '77';
      // var invoiceYear = '99';
      accountNumber = 0;
      // var dateWritten = '123456';
      var moveFlag = '0';
      // var dValue = '';
      // var eValue = '';
      // var fDate = '';
      var d = new Date();
      var todaysDay = d.getDate();
      var todaysMonth = d.getMonth() + 1;
      var todaysYear = d.getFullYear();
      var formatDate = dateToNumber(d);

      for (i = 0; i < data.length; i++) {
        if (data[i]['A']) vendorNumber = data[i]['A'];

        if (data[i]['B']) invoiceNumber = data[i]['B'];

        // if (data[i]['C']) typeCode = data[i]['C'];

        if (data[i]['D']) invoiceAmount = data[i]['D'];

        if (data[i]['E']) accountNumber = data[i]['E'];

        if (data[i]['F']) fDate = data[i]['F'];

        var sqlquery =
          'insert into TYLER.APIMPRT' +
          '(apivnd, apiinv, apicod, apidmo, apiddy, apidyr, apigos, apidsc, apinet, apiimo, apiidy, apiiyr, apiact, apidwr, apiflg)' +
          'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with NONE';

        writeAPIMPRT = pjs.query(sqlquery, [
          vendorNumber,
          invoiceNumber,
          typeCode,
          dueMonth,
          dueDay,
          dueYear,
          invoiceAmount,
          discountAmount,
          netAmount,
          todaysMonth,
          todaysDay,
          todaysYear,
          accountNumber,
          formatDate,
          moveFlag,
        ]);

        // console.log(
        //   'APIVND: ' + vendorNumber,
        //   'APIINV: ' + invoiceNumber,
        //   'APIGOS: ' + invoiceAmount,
        //   'APIACT: ' + accountNumber,
        //   'APIIMO: ' + todaysMonth,
        //   'APIIDY: ' + todaysDay,
        //   'APIIYR: ' + todaysYear,
        //   'APIDWR: ' + formatDate
        // );
      }
      console.log('Records updated.');
    }

    if (showGrid) {
      doGrid();
    }

    function doGrid() {
      Exit = false;
      Exit2 = false;

      while (!Exit && !Exit2) {
        console.log('Show Grid clicked.');
        display.APIMPRTGrid.execute();

        // console.log(vendorNumber,
        //   invoiceNumber,
        //   typeCode,
        //   dueMonth,
        //   dueDay,
        //   dueYear,
        //   invoiceAmount,
        //   discountAmount,
        //   netAmount,
        //   todaysMonth,
        //   todaysDay,
        //   todaysYear,
        //   accountNumber,
        //   formatDate,
        //   moveFlag,);

        if (Exit2) {
          return;
        }

        if (clear) {
          pjs.query('delete from TYLER.APIMPRT');
          console.log('table cleared');
        }
      }
    }
  }
}
exports.run = importxlsx;

// store number (APIVND) AP store# with 9 infront
// inv num (APIINV)
// C Date (ignore for now)
// D (gross amt APIGOS)
// E (accnt # APIACT )
// F Date
// all dates get todays date
// f= DMO,DDY,DYR,IMO,IDY,IYR

function dateToNumber(theDate) {
  var year = theDate.getFullYear() % 2000;
  var month = theDate.getMonth() + 1;
  var day = theDate.getDate();
  var number = month * 10000 + day * 100 + year;
  return number;
}
